package com.minecraftonline.penguindungeons.wand;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.util.HeldItemListener;
import org.spongepowered.api.data.type.HandType;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;

public class PDWandHeldListener extends HeldItemListener {
    @Override
    public boolean isDesiredItem(ItemStackSnapshot itemStack) {
        return PDWand.isPDWand(itemStack);
    }

    @Override
    public void onStartHolding(Player player, ItemStackSnapshot wand, HandType hand) {
        PDWand.beginHolding(player, wand, hand);
    }

    @Override
    public void onStopHolding(Player player, ItemStackSnapshot wand, HandType hand) {
        PDWand.stopHolding(player, wand, hand);
    }

    @Override
    public void onSwapHands(Player player) {
        PenguinDungeons.getLogger().info(player.getName() + " swapped hands");
        // TODO: This should probably actually be implemented, or removed if not needed
    }
}
