package com.minecraftonline.penguindungeons.gui.inventory;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class RowConfigurationButton {

    private final ItemStack icon;
    private final Consumer<Player> onClick;
    private final Supplier<Boolean> shouldApply;
    private final ItemStack notAppliedItem;

    private RowConfigurationButton(final ItemStack icon, final Consumer<Player> onClick, Supplier<Boolean> shouldApply, ItemStack notAppliedItem) {
        this.icon = icon;
        this.onClick = onClick;
        this.shouldApply = shouldApply;
        this.notAppliedItem = notAppliedItem;
    }

    public static RowConfigurationButton of(final ItemStack icon, final Consumer<Player> onClick) {
        return of(icon, onClick, () -> true);
    }

    public static RowConfigurationButton of(final ItemStack icon, final Consumer<Player> onClick, Supplier<Boolean> shouldApply) {
        return of(icon, onClick, shouldApply, null);
    }

    public static RowConfigurationButton of(final ItemStack icon, final Consumer<Player> onClick, Supplier<Boolean> shouldApply, ItemStack notAppliedItem) {
        return new RowConfigurationButton(icon, onClick, shouldApply, notAppliedItem);
    }

    public ItemStack getIcon() {
        return icon;
    }

    @Nullable
    public ItemStack getNotAppliedItem() {
        return notAppliedItem;
    }

    public void onClick(Player player) {
        this.onClick.accept(player);
    }

    public boolean shouldApply() {
        return shouldApply.get();
    }
}
