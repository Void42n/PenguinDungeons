package com.minecraftonline.penguindungeons.gui.inventory;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.item.inventory.ClickInventoryEvent;
import org.spongepowered.api.event.item.inventory.InteractInventoryEvent;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.InventoryArchetypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.property.InventoryTitle;
import org.spongepowered.api.item.inventory.property.SlotIndex;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;

import javax.annotation.Nullable;
import java.util.Optional;

public class ConfirmationPopup {

    private final ItemStack confirmButton;
    private final Runnable onConfirm;
    private final Text title;
    @Nullable
    private final Inventory returnToInventory;

    public ConfirmationPopup(final ItemStack confirmButton, final Runnable onConfirm, Text title, @Nullable final Inventory returnToInventory) {
        this.confirmButton = confirmButton;
        this.onConfirm = onConfirm;
        this.title = title;
        this.returnToInventory = returnToInventory;
    }

    public Inventory create() {
        Inventory inventory = Inventory.builder()
                .of(InventoryArchetypes.MENU_ROW)
                .property(InventoryTitle.of(this.title))
                .listener(InteractInventoryEvent.class, event -> {
                    Optional<Player> optPlayer = event.getCause().first(Player.class);
                    if (!optPlayer.isPresent()) {
                        return;
                    }
                    if (event instanceof InteractInventoryEvent.Close) {
                        if (this.returnToInventory != null) {
                            Task.builder()
                                    .delayTicks(1)
                                    .execute(() -> optPlayer.get().openInventory(this.returnToInventory))
                                    .submit(PenguinDungeons.getInstance());
                        }
                    }
                    if (event instanceof ClickInventoryEvent) {
                        event.setCancelled(true);
                        Optional<Slot> slot = ((ClickInventoryEvent) event).getSlot();
                        if (!slot.isPresent()) {
                            return;
                        }
                        Optional<Integer> optIndex = slot.get().getInventoryProperty(SlotIndex.class).map(SlotIndex::getValue);
                        if (optIndex.isPresent() && optIndex.get() == 0) {
                            this.onConfirm.run();
                            Task.builder()
                                    .delayTicks(1)
                                    .execute(() -> {
                                        optPlayer.get().closeInventory();
                                        if (this.returnToInventory != null) {
                                            optPlayer.get().openInventory(this.returnToInventory);
                                        }
                                    })
                                    .submit(PenguinDungeons.getInstance());

                        }
                    }
                })
                .build(PenguinDungeons.getInstance());
        inventory.offer(this.confirmButton);
        return inventory;
    }
}
