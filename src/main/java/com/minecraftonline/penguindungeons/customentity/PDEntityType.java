package com.minecraftonline.penguindungeons.customentity;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.entity.EntityArchetype;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.world.World;

import java.util.Optional;

public interface PDEntityType {

    ResourceKey getId();

    default String getPermission() {
        return PenguinDungeons.BASE_PERMISSION + "entity." + getId().formattedWith(".");
    }

    ItemStack getSpawnEgg();

    /**
     * Creates an entity of this type.
     * @param world World to spawn.
     * @param pos Position to spawn
     * @return Entity, using this type.
     */
    Spawnable createEntity(World world, Vector3d pos);

    default Optional<EntityArchetype> getEntityArchetype() {
        return Optional.empty();
    }
}
