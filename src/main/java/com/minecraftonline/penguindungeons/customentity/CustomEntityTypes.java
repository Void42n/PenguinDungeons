package com.minecraftonline.penguindungeons.customentity;

import com.google.common.collect.ImmutableSet;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.customentity.golem.ExplodingSnowGolem;
import com.minecraftonline.penguindungeons.customentity.golem.IceSnowGolem;
import com.minecraftonline.penguindungeons.customentity.golem.SnowGolemType;
import com.minecraftonline.penguindungeons.customentity.golem.SnowflakeGolem;
import com.minecraftonline.penguindungeons.customentity.shulker.ChaosShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.ShulkerType;
import com.minecraftonline.penguindungeons.customentity.shulker.potion.PotionEffectShulkers;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.BlindingShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.ExperienceShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.ExplodingShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.FireyShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.GlowingShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.HealthyShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.InvisibleShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.NormalShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.PaladinShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.RainbowShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.RegeneratingShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.SniperShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.SpikeyShulker;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.data.value.ValueContainer;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@SuppressWarnings("unused")
public class CustomEntityTypes {

    private static Map<ResourceKey, CustomEntityType> REGISTRY = null;

    public static final ShulkerType NORMAL_SHULKER         = new NormalShulker(ResourceKey.pd("shulker_normal"));
    public static final ShulkerType HEALTHY_SHULKER        = new HealthyShulker(ResourceKey.pd("shulker_healthy"));
    public static final ShulkerType GLOWING_SHULKER        = new GlowingShulker(ResourceKey.pd("shulker_glowing"));
    public static final ShulkerType INVISIBLE_SHULKER      = new InvisibleShulker(ResourceKey.pd("shulker_invisible"));
    public static final ShulkerType REGENERATING_SHULKER   = new RegeneratingShulker(ResourceKey.pd("shulker_regenerating"));
    public static final ShulkerType SNIPER_SHULKER         = new SniperShulker(ResourceKey.pd("shulker_sniper"));
    public static final ShulkerType EXPLODING_SHULKER      = new ExplodingShulker(ResourceKey.pd("shulker_exploding"));
    public static final ShulkerType FIREY_SHULKER          = new FireyShulker(ResourceKey.pd("shulker_firey"));
    public static final ShulkerType SPIKEY_SHULKER         = new SpikeyShulker(ResourceKey.pd("shulker_spikey"));
    public static final ShulkerType BLINDING_SHULKER       = new BlindingShulker(ResourceKey.pd("shulker_blinding"));
    public static final ShulkerType POISON_SHULKER         = PotionEffectShulkers.poison(ResourceKey.pd("shulker_poison"));
    public static final ShulkerType JUMP_BOOST_SHULKER     = PotionEffectShulkers.jumpBoost(ResourceKey.pd("shulker_jump_boost"));
    public static final ShulkerType MINING_FATIGUE_SHULKER = PotionEffectShulkers.miningFatigue(ResourceKey.pd("shulker_mining_fatigue"));
    public static final ShulkerType SLOWNESS_SHULKER       = PotionEffectShulkers.slowness(ResourceKey.pd("shulker_slowness"));
    public static final ShulkerType SPEED_SHULKER          = PotionEffectShulkers.speed(ResourceKey.pd("shulker_speed"));
    public static final ShulkerType EXPERIENCE_SHULKER     = new ExperienceShulker(ResourceKey.pd("shulker_experience"));
    public static final ShulkerType PALADIN_SHULKER        = new PaladinShulker(ResourceKey.pd("shulker_paladin"));
    public static final ShulkerType RAINBOW_SHULKER        = new RainbowShulker(ResourceKey.pd("shulker_rainbow"));
    public static final ShulkerType CHAOS_SHULKER          = new ChaosShulker(ResourceKey.pd("shulker_chaos"));
    public static final SnowGolemType SNOWFLAKE_GOLEM      = new SnowflakeGolem(ResourceKey.pd("snow_golem_snowflake"));
    public static final SnowGolemType EXPLODING_SNOW_GOLEM = new ExplodingSnowGolem(ResourceKey.pd("snow_golem_explode"));
    public static final SnowGolemType ICE_SNOW_GOLEM       = new IceSnowGolem(ResourceKey.pd("snow_golem_ice"));
    public static final CustomPolarBear POLAR_BEAR         = new CustomPolarBear(ResourceKey.pd("polar_bear_custom"));

    public static void initRegistry() {
        if (REGISTRY != null) {
            throw new IllegalStateException("Registry is already initialised!");
        }
        REGISTRY = new HashMap<>();
        for (Field field : CustomEntityTypes.class.getFields()) {
            if (!CustomEntityType.class.isAssignableFrom(field.getType())) {
                return;
            }
            try {
                CustomEntityType entityType = (CustomEntityType) field.get(null);
                REGISTRY.put(entityType.getId(), entityType);
            } catch (IllegalAccessException e) {
                PenguinDungeons.getLogger().error("Error initialising registry", e);
            }
        }
    }

    public static Optional<CustomEntityType> getById(final ResourceKey key) {
        return Optional.of(REGISTRY.get(key));
    }

    public static Optional<CustomEntityType> get(ValueContainer<?> valueContainer) {
        return valueContainer.get(PenguinDungeonKeys.PD_ENTITY_TYPE).flatMap(CustomEntityTypes::getById);
    }

    public static Collection<CustomEntityType> getAll() {
        return ImmutableSet.copyOf(REGISTRY.values());
    }
}
