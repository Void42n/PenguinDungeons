package com.minecraftonline.penguindungeons.customentity.shulker.potion;

import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Collections;

public class PotionEffectShulkers {

    private static final int TICKS_PER_SECOND = 20;

    public static PotionEffectShulker jumpBoost(final ResourceKey key) {
        return SimplePotionEffectShulker.builder()
                .key(key)
                .displayName(Text.of(TextColors.BLUE, "Jump Boost", TextColors.WHITE, " Shulker"))
                .description(Collections.singletonList(Text.of(TextColors.WHITE, "A shulker that applies ", TextColors.BLUE, "jump boost", TextColors.WHITE, " to its targets.")))
                .color(DyeColors.LIGHT_BLUE)
                .effect(PotionEffectTypes.JUMP_BOOST)
                .amplifier(2)
                .duration(TICKS_PER_SECOND*3)
                .build();
    }

    public static PotionEffectShulker miningFatigue(final ResourceKey key) {
        return SimplePotionEffectShulker.builder()
                .key(key)
                .displayName(Text.of(TextColors.BLUE, "Mining Fatigue", TextColors.WHITE, " Shulker"))
                .description(Collections.singletonList(Text.of(TextColors.WHITE, "A shulker that applies ", TextColors.BLUE, "mining fatigue", TextColors.WHITE, " to targets.")))
                .color(DyeColors.CYAN)
                .effect(PotionEffectTypes.MINING_FATIGUE)
                .amplifier(1)
                .duration(TICKS_PER_SECOND*15)
                .build();
    }

    public static PotionEffectShulker poison(final ResourceKey key) {
        return SimplePotionEffectShulker.builder()
                .key(key)
                .displayName(Text.of(TextColors.DARK_GREEN, "Poison", TextColors.WHITE, " Shulker"))
                .description(Collections.singletonList(Text.of(TextColors.WHITE, "A shulker that ", TextColors.DARK_GREEN, "poisons", TextColors.WHITE, " targets.")))
                .color(DyeColors.GREEN)
                .effect(PotionEffectTypes.POISON)
                .amplifier(1)
                .duration(TICKS_PER_SECOND*5)
                .build();
    }

    public static PotionEffectShulker slowness(final ResourceKey key) {
        return SimplePotionEffectShulker.builder()
                .key(key)
                .displayName(Text.of(TextColors.DARK_RED, "Slowness", TextColors.WHITE, " Shulker"))
                .description(Collections.singletonList(Text.of(TextColors.WHITE, "A shulker that ", TextColors.DARK_RED, "slows", TextColors.WHITE, " targets.")))
                .color(DyeColors.BROWN)
                .effect(PotionEffectTypes.SLOWNESS)
                .amplifier(2)
                .duration(TICKS_PER_SECOND*10)
                .build();
    }

    public static PotionEffectShulker speed(final ResourceKey key) {
        return SimplePotionEffectShulker.builder()
                .key(key)
                .displayName(Text.of(TextColors.AQUA, "Speed", TextColors.WHITE, " Shulker"))
                .description(Collections.singletonList(Text.of(TextColors.WHITE, "A shulker that ", TextColors.GRAY, "speeds up", TextColors.WHITE, " targets")))
                .color(DyeColors.LIGHT_BLUE)
                .effect(PotionEffectTypes.SPEED)
                .amplifier(1)
                .duration(TICKS_PER_SECOND*3)
                .build();
    }
}
