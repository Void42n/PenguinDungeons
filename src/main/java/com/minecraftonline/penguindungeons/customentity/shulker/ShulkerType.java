package com.minecraftonline.penguindungeons.customentity.shulker;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.golem.Shulker;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

public abstract class ShulkerType extends AbstractCustomEntity {

    public ShulkerType(ResourceKey key) {
        super(key);
    }

    public abstract DyeColor getColor();

    public abstract Text getDisplayName();

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, EntityTypes.SHULKER)
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        return Spawnable.of(makeShulker(world, blockPos), this::onLoad);
    }

    protected Shulker makeShulker(World world, Vector3d blockPos) {
        Shulker shulker = (Shulker) world.createEntity(EntityTypes.SHULKER, blockPos);
        shulker.offer(Keys.DISPLAY_NAME, getDisplayName());
        shulker.offer(Keys.DYE_COLOR, getColor());
        shulker.offer(new PDEntityTypeData(getId()));
        shulker.offer(new OwnerLootData(ownerLoot()));
        applyEquipment(shulker);
        applyLootTable(shulker);
        // don't attack other mobs such as shulkers
        Goal<Agent> targetGoals = shulker.getGoal(GoalTypes.TARGET).get();
        targetGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIHurtByTarget)
            .forEach(task -> targetGoals.removeTask((AITask<? extends Agent>) task));
        return shulker;
    }

    public boolean nameVisibleOnEntity() {
        return true;
    }

    public boolean ownerLoot()
    {
        return true;
    }
}
