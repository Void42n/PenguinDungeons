package com.minecraftonline.penguindungeons.customentity.shulker.standard;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.Collections;
import java.util.List;

public class InvisibleShulker extends StandardShulkerType {

    public InvisibleShulker(ResourceKey key) {
        super(key);
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A shulker that is ", TextColors.GREEN, "invisible"));
    }

    @Override
    public DyeColor getColor() {
        return DyeColors.WHITE; // Doesn't matter much.
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.WHITE, "Invisible Shulker");
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        Entity shulker = super.makeShulker(world, pos);
        shulker.offer(Keys.POTION_EFFECTS, Collections.singletonList(PotionEffect.of(PotionEffectTypes.INVISIBILITY, 1, Integer.MAX_VALUE)));
        return Spawnable.of(shulker);
    }
}
