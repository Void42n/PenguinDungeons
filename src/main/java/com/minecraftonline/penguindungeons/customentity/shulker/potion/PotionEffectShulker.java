package com.minecraftonline.penguindungeons.customentity.shulker.potion;

import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.StandardShulkerType;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.task.AITaskType;
import org.spongepowered.api.entity.living.golem.Shulker;

/**
 * Abstract class for a shulker that applies a potion effect
 * on bullet hit.
 */
public abstract class PotionEffectShulker extends StandardShulkerType {

    private final AITaskType aiTaskType;

    public PotionEffectShulker(ResourceKey key, AITaskType aiTaskType) {
        super(key);
        this.aiTaskType = aiTaskType;
    }

    @Override
    public void onLoad(Entity entity) {
        if (!(entity instanceof Shulker)) {
            return;
        }
        Shulker shulker = (Shulker) entity;
        AIUtil.swapShulkerAttackForCustom(shulker, AIUtil.ShulkerBulletCreator.forCustomEffect(getPotionEffect()), this.aiTaskType);
    }

    protected abstract PotionEffect getPotionEffect();
}
