package com.minecraftonline.penguindungeons.customentity.shulker.standard;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.AbstractAITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.golem.Shulker;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BlindingShulker extends StandardShulkerType {

    public BlindingShulker(ResourceKey key) {
        super(key);
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A shulker that ", TextColors.GRAY, "blinds", TextColors.WHITE, " you"));
    }

    @Override
    public DyeColor getColor() {
        return DyeColors.BLACK;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.GRAY, "Blinding", TextColors.WHITE, " Shulker");
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        Entity shulker = super.makeShulker(world, blockPos);
        onLoad(shulker);
        return Spawnable.of(shulker);
    }

    @Override
    public void onLoad(Entity entity) {
        if (!(entity instanceof Shulker)) {
            return;
        }
        Shulker shulker = (Shulker) entity;

        Goal<Agent> goal = shulker.getGoal(GoalTypes.NORMAL).get();

        goal.addTask(3, new BlindTargetAI());
    }

    public static class BlindTargetAI extends AbstractAITask<Shulker> {

        public BlindTargetAI() {
            super(PenguinDungeonAITaskTypes.SHULKER_BLINDING_ATTACK);
        }

        private Instant nextApply;

        @Override
        public void start() {
            nextApply = Instant.now().plus(1, ChronoUnit.SECONDS);
        }

        @Override
        public boolean shouldUpdate() {
            // Don't update if they already have the effect.
            return getOwner().isPresent() && getOwner().get().getTarget().isPresent()
                    && !getOwner().get().getTarget().get().get(Keys.POTION_EFFECTS)
                    .flatMap(effects -> effects.stream().filter(effect -> effect.getType() == PotionEffectTypes.BLINDNESS).findAny()).isPresent();
        }

        @Override
        public void update() {
            if (nextApply.isAfter(Instant.now())) {
                return;
            }
            Entity target = getOwner().get().getTarget().get();
            List<PotionEffect> effects = target.get(Keys.POTION_EFFECTS).orElse(new ArrayList<>());
            effects.add(PotionEffect.of(PotionEffectTypes.BLINDNESS, 1, (int) (Sponge.getServer().getTicksPerSecond() * 8)));
            target.offer(Keys.POTION_EFFECTS, effects);
            nextApply = Instant.now().plus(10, ChronoUnit.SECONDS);
        }

        @Override
        public boolean continueUpdating() {
            return getOwner().isPresent() && getOwner().get().getTarget().isPresent();
        }

        @Override
        public void reset() {}

        @Override
        public boolean canRunConcurrentWith(AITask<Shulker> other) {
            return true;
        }

        @Override
        public boolean canBeInterrupted() {
            return true;
        }
    }
}
