package com.minecraftonline.penguindungeons.customentity.shulker.standard;

import com.minecraftonline.penguindungeons.customentity.shulker.ShulkerType;
import com.minecraftonline.penguindungeons.equipment.EquipmentLoadout;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public abstract class StandardShulkerType extends ShulkerType {

    public StandardShulkerType(ResourceKey key) {
        super(key);
    }

    @Override
    public EquipmentLoadout.Builder makeEquipmentLoadout() {
        return EquipmentLoadout.builder();
    }
}
