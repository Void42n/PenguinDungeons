package com.minecraftonline.penguindungeons.customentity.shulker.standard;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.Collections;
import java.util.List;

public class GlowingShulker extends StandardShulkerType {

    public GlowingShulker(ResourceKey key) {
        super(key);
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A shulker that ", TextColors.YELLOW, "glows"));
    }

    @Override
    public DyeColor getColor() {
        return DyeColors.YELLOW;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.YELLOW, "Glowing ", TextColors.WHITE, "Shulker");
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        Entity shulker = super.makeShulker(world, blockPos);
        shulker.offer(Keys.GLOWING, true);
        return Spawnable.of(shulker);
    }
}
