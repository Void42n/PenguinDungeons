package com.minecraftonline.penguindungeons.customentity.shulker.standard;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.monster.EntityShulker;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.living.golem.Shulker;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.Arrays;
import java.util.List;

public class SniperShulker extends StandardShulkerType {

    public SniperShulker(ResourceKey key) {
        super(key);
    }

    @Override
    public List<Text> getEntityDescription() {
        return Arrays.asList(Text.of(TextColors.WHITE, "A shulker that shoots bullets"),
                Text.of(TextColors.WHITE, "from a long way away."));
    }

    @Override
    public DyeColor getColor() {
        return DyeColors.GRAY;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.DARK_GRAY, "Sniper", TextColors.WHITE, " Shulker");
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        Shulker shulker = super.makeShulker(world, blockPos);

        // Double.
        EntityShulker mcShulker = (EntityShulker) shulker;
        mcShulker.getEntityAttribute(SharedMonsterAttributes.FOLLOW_RANGE).applyModifier(new AttributeModifier("Sniper shulker", 2, 1));

        return Spawnable.of(shulker);
    }
}
