package com.minecraftonline.penguindungeons.customentity.golem;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.golem.Shulker;
import org.spongepowered.api.entity.living.golem.SnowGolem;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.ai.AIUtil.ShulkerBulletCreator;
import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.ai.EntityAIAttackRanged;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.monster.EntitySnowman;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityShulkerBullet;
import net.minecraft.util.EnumFacing.Axis;
import net.minecraft.util.math.MathHelper;

public class SnowflakeGolem extends SnowGolemType {

    private static final int TICKS_PER_SECOND = 20;

    public SnowflakeGolem(ResourceKey key) {
        super(key);
    }

    public Text getDisplayName() {
        return Text.of(TextColors.WHITE, "Snowflake Golem");
    }

    public boolean wearPumpkin() {
        return false;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A snow golem that shoots straight shulker bullets"));
    }

    @Override
    public void onLoad(Entity entity) {
        if (!(entity instanceof SnowGolem)) {
            throw new IllegalArgumentException("Expected a SnowGolem to be given to SnowflakeGolem to load, but got: " + entity);
        }
        SnowGolem snowGolem = (SnowGolem) entity;

        Goal<Agent> normalGoals = snowGolem.getGoal(GoalTypes.NORMAL).get();
        // Remove regular attack.
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIAttackRanged)
                .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        normalGoals.addTask(1, new RangedAttack(snowGolem));

        // attack players
        Goal<Agent> targetGoals = snowGolem.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget((EntityCreature) entity, EntityPlayer.class, true));
    }

    public static class RangedAttack extends DelegatingToMCAI<SnowGolem> {

        public RangedAttack(SnowGolem snowGolem) {
            super(PenguinDungeonAITaskTypes.SNOW_GOLEM_RANGED_ATTACK, new MCRangedAttack((IRangedAttackMob) snowGolem));
        }
    }

    public static class MCRangedAttack extends EntityAIAttackRanged {

        /** The entity the AI instance has been applied to */
        private final EntityLiving entityHost;
        /** The entity (as a RangedAttackMob) the AI instance has been applied to. */
        private final IRangedAttackMob rangedAttackEntityHost;
        private EntityLivingBase attackTarget;
        /**
         * A decrementing tick that spawns a ranged attack once this value reaches 0. It is then set back to the
         * maxRangedAttackTime.
         */
        private int rangedAttackTime;
        private final double entityMoveSpeed;
        private int seeTime;
        private final int attackIntervalMin;
        /** The maximum time the AI has to wait before peforming another ranged attack. */
        private final int maxRangedAttackTime;
        private final float attackRadius;
        private final float maxAttackDistance;

        public MCRangedAttack(IRangedAttackMob attacker) {
            this(attacker, 1.25D, 25, 45, 15.0F);
        }

        public MCRangedAttack(IRangedAttackMob attacker, double movespeed, int maxAttackTime, float maxAttackDistanceIn) {
           this(attacker, movespeed, maxAttackTime, maxAttackTime, maxAttackDistanceIn);
        }

        public MCRangedAttack(IRangedAttackMob attacker, double movespeed, int p_i1650_4_, int maxAttackTime, float maxAttackDistanceIn) {
           super(attacker, movespeed, p_i1650_4_, maxAttackTime, maxAttackDistanceIn);
           this.rangedAttackTime = -1;
           if (!(attacker instanceof EntityLivingBase)) {
              throw new IllegalArgumentException("ArrowAttackGoal requires Mob implements RangedAttackMob");
           } else {
              this.rangedAttackEntityHost = attacker;
              this.entityHost = (EntityLiving)attacker;
              this.entityMoveSpeed = movespeed;
              this.attackIntervalMin = p_i1650_4_;
              this.maxRangedAttackTime = maxAttackTime;
              this.attackRadius = maxAttackDistanceIn;
              this.maxAttackDistance = maxAttackDistanceIn * maxAttackDistanceIn;
              this.setMutexBits(3);
           }
        }

        /**
         * Returns whether the EntityAIBase should begin execution.
         */
        public boolean shouldExecute() {
           EntityLivingBase entitylivingbase = this.entityHost.getAttackTarget();
           if (entitylivingbase == null) {
              return false;
           } else {
              this.attackTarget = entitylivingbase;
              return true;
           }
        }

        /**
         * Returns whether an in-progress EntityAIBase should continue executing
         */
        public boolean shouldContinueExecuting() {
           return this.shouldExecute() || !this.entityHost.getNavigator().noPath();
        }

        /**
         * Reset the task's internal state. Called when this task is interrupted by another one
         */
        public void resetTask() {
           this.attackTarget = null;
           this.seeTime = 0;
           this.rangedAttackTime = -1;
        }

        /**
         * Keep ticking a continuous task that has already been started
         */
        public void updateTask() {
           double d0 = this.entityHost.getDistanceSq(this.attackTarget.posX, this.attackTarget.getEntityBoundingBox().minY, this.attackTarget.posZ);
           boolean flag = this.entityHost.getEntitySenses().canSee(this.attackTarget);
           if (flag) {
              ++this.seeTime;
           } else {
              this.seeTime = 0;
           }

           if (d0 <= (double)this.maxAttackDistance && this.seeTime >= 20) {
              this.entityHost.getNavigator().clearPath();
           } else {
              this.entityHost.getNavigator().tryMoveToEntityLiving(this.attackTarget, this.entityMoveSpeed);
           }

           this.entityHost.getLookHelper().setLookPositionWithEntity(this.attackTarget, 30.0F, 30.0F);
           if (--this.rangedAttackTime == 0) {
              if (!flag) {
                 return;
              }

              float f = MathHelper.sqrt(d0) / this.attackRadius;

              // use snowflakes
              double d1 = this.attackTarget.posX - this.entityHost.posX;
              double d2 = this.attackTarget.getEntityBoundingBox().minY + (double)(this.attackTarget.height / 2.0F) - (this.entityHost.posY + (double)(this.entityHost.height / 2.0F));
              double d3 = this.attackTarget.posZ - this.entityHost.posZ;
              EntitySnowman mcSnowGolem = (EntitySnowman) this.rangedAttackEntityHost;
              PotionEffect potion = PotionEffect.of(PotionEffectTypes.SLOWNESS, 1, TICKS_PER_SECOND*10);
              ShulkerBulletCreator shulkerBulletCreator = AIUtil.ShulkerBulletCreator.forDirectSnowFlake(mcSnowGolem, AIUtil.ShulkerBulletCreator.forHit(potion), d1, d2, d3);
              EntityShulkerBullet entityshulkerbullet = shulkerBulletCreator.create(mcSnowGolem.world, mcSnowGolem, this.attackTarget, Axis.Y);
              entityshulkerbullet.setCustomNameTag("Snowflake");
              mcSnowGolem.world.spawnEntity(entityshulkerbullet);
              this.rangedAttackTime = MathHelper.floor(f * (float)(this.maxRangedAttackTime - this.attackIntervalMin) + (float)this.attackIntervalMin);
           } else if (this.rangedAttackTime < 0) {
              float f2 = MathHelper.sqrt(d0) / this.attackRadius;
              this.rangedAttackTime = MathHelper.floor(f2 * (float)(this.maxRangedAttackTime - this.attackIntervalMin) + (float)this.attackIntervalMin);
           }

        }
    }
}
