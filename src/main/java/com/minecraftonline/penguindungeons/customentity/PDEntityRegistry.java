package com.minecraftonline.penguindungeons.customentity;

import com.minecraftonline.penguindungeons.registry.PDRegistry;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.EntityType;

public class PDEntityRegistry extends PDRegistry<PDEntityType> {

    public PDEntityRegistry() {
        registerFallback(key -> Sponge.getRegistry().getType(EntityType.class, key.asString()).map(SimpleEntityType::new));
    }

    public void registerAll(Iterable<? extends PDEntityType> entities) {
        entities.forEach(entity -> register(entity.getId(), entity));
    }

}
