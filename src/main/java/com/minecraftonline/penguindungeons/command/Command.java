package com.minecraftonline.penguindungeons.command;

import org.spongepowered.api.command.spec.CommandSpec;

public interface Command {
    CommandSpec create();
}
