package com.minecraftonline.penguindungeons.command.commands;

import com.minecraftonline.penguindungeons.command.AbstractCommand;
import com.minecraftonline.penguindungeons.command.PDArguments;
import com.minecraftonline.penguindungeons.dungeon.Dungeon;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.text.Text;

public class SpawnDungeonCommand extends AbstractCommand {

    public SpawnDungeonCommand() {
        super(DungeonCommand.BASE_PERMISSION + "spawn");
        Text DUNGEON_KEY = Text.of("dungeon");

        addArguments(PDArguments.dungeon(DUNGEON_KEY));
        setExecutor((src, args) -> {
            final Dungeon dungeon = args.requireOne(DUNGEON_KEY);
            dungeon.spawn(true);
            src.sendMessage(Text.of("Spawned Dungeon."));
            return CommandResult.success();
        });
    }
}
