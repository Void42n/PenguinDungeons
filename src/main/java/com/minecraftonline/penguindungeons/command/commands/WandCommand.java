package com.minecraftonline.penguindungeons.command.commands;

import com.minecraftonline.penguindungeons.command.AbstractCommand;
import com.minecraftonline.penguindungeons.command.PDArguments;
import com.minecraftonline.penguindungeons.dungeon.Dungeon;
import com.minecraftonline.penguindungeons.wand.PDWand;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.entity.MainPlayerInventory;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.text.Text;

public class WandCommand extends AbstractCommand {

    public WandCommand() {
        super(PDWand.BASE_PERMISSION + ".command");

        Text DUNGEON_KEY = Text.of("dungeon");

        addArguments(PDArguments.dungeon(DUNGEON_KEY));
        setExecutor((src, args) -> {
            if (!(src instanceof Player)) {
                throw new CommandException(Text.of("You must be a player to use this command!"));
            }
            final Dungeon dungeon = args.requireOne(DUNGEON_KEY);
            final ItemStack wandItem = PDWand.createFor(dungeon.name());
            final Player player = (Player)src;
            final Inventory inv = player.getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(MainPlayerInventory.class));
            if (!inv.canFit(wandItem)) {
                throw new CommandException(Text.of("No room in your inventory to fit a wand!"));
            }
            inv.offer(wandItem);
            PDWand.refreshHeldAdded(player);
            return CommandResult.success();
        });
    }
}
