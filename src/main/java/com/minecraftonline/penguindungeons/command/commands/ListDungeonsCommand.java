package com.minecraftonline.penguindungeons.command.commands;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.command.AbstractCommand;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.service.pagination.PaginationList;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.List;
import java.util.stream.Collectors;

public class ListDungeonsCommand extends AbstractCommand {

    public ListDungeonsCommand() {
        super(DungeonCommand.BASE_PERMISSION + "list");
        setExecutor((src, args) -> {
            List<Text> contents = PenguinDungeons.getDungeons().values().stream()
                    .map(dungeon -> Text.of(TextColors.BLUE, dungeon.name(), TextColors.GRAY, " - " + dungeon.spawnLocations().size() + " nodes"))
                    .collect(Collectors.toList());
            PaginationList pages = PaginationList.builder()
                    .title(Text.of("Dungeons"))
                    .contents(contents)
                    .build();
            pages.sendTo(src);
            return CommandResult.success();
        });
    }
}
