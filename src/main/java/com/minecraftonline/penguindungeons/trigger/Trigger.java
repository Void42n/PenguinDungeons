package com.minecraftonline.penguindungeons.trigger;

import java.util.List;

public interface Trigger {

    void register(Runnable onTrigger);

    List<String> getDungeonsToSpawn();

    TriggerType getType();
}
