package com.minecraftonline.penguindungeons.config.serializer;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.reflect.TypeToken;
import com.minecraftonline.penguindungeons.dungeon.Dungeon;
import com.minecraftonline.penguindungeons.dungeon.WeightedSpawnOption;
import com.minecraftonline.penguindungeons.util.PDTypeTokens;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.util.TypeTokens;

import java.util.Map;
import java.util.UUID;

public class DungeonSerializer implements TypeSerializer<Dungeon> {
    @Override
    public @Nullable Dungeon deserialize(@NonNull TypeToken<?> type, @NonNull ConfigurationNode value) throws ObjectMappingException {
        final UUID world = value.getNode("world").getValue(TypeTokens.UUID_TOKEN);
        final String name = value.getNode("name").getString();
        if (world == null) {
            throw new ObjectMappingException("Missing world field for dungeon!");
        }
        if (name == null) {
            throw new ObjectMappingException("Missing name field for dungeon!");
        }
        Dungeon dungeon = new Dungeon(name, world);

        final ConfigurationNode entities = value.getNode("entities");
        for (Map.Entry<Object, ? extends ConfigurationNode> entry : entities.getChildrenMap().entrySet()) {
            String vec3dStr = entry.getKey().toString();
            if (vec3dStr == null) {
                continue;
            }
            String[] vec3dSplit = vec3dStr.split(",", 3);
            final Vector3d vector3d;
            try {
                vector3d = Vector3d.from(
                        Double.parseDouble(vec3dSplit[0]),
                        Double.parseDouble(vec3dSplit[1]),
                        Double.parseDouble(vec3dSplit[2])
                );
            } catch (NumberFormatException e) {
                throw new ObjectMappingException("Invalid vec3d key: " + vec3dStr, e);
            }
            WeightedSpawnOption weightedSpawnOption = entry.getValue().getValue(PDTypeTokens.WEIGHTED_SPAWN_OPTION_TOKEN);
            dungeon.set(vector3d, weightedSpawnOption);
        }
        return dungeon;
    }

    @Override
    public void serialize(@NonNull TypeToken<?> type, @Nullable Dungeon obj, @NonNull ConfigurationNode value) throws ObjectMappingException {
        if (obj == null) {
            return;
        }
        value.getNode("world").setValue(TypeTokens.UUID_TOKEN, obj.world());
        value.getNode("name").setValue(obj.name());
        final ConfigurationNode entities = value.getNode("entities");
        entities.setValue(null); // Blank before we write to stop us only ever adding.
        for (Map.Entry<Vector3d, WeightedSpawnOption> entry : obj.spawnLocations().entrySet()) {
            final Vector3d pos = entry.getKey();
            entities.getNode(pos.getX() + "," + pos.getY() + "," + pos.getZ()).setValue(PDTypeTokens.WEIGHTED_SPAWN_OPTION_TOKEN, entry.getValue());
        }
    }
}
