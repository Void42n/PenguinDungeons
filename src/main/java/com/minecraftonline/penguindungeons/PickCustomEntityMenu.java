package com.minecraftonline.penguindungeons;

import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.gui.inventory.DualScrollableInventoryList;
import com.minecraftonline.penguindungeons.gui.inventory.ScrollableInventoryList;
import com.minecraftonline.penguindungeons.gui.inventory.ScrollableInventoryListInfo;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.entity.MainPlayerInventory;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.service.permission.Subject;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.List;
import java.util.stream.Collectors;

public class PickCustomEntityMenu {

    public static Inventory createForSubject(Subject subject) {

        List<ItemStack> builtInCustomEntities = CustomEntityTypes.getAll().stream()
                .filter(customEntity -> subject.hasPermission(customEntity.getPermission()))
                .map(PDEntityType::getSpawnEgg)
                .collect(Collectors.toList());

        List<ItemStack> configCustomEntities = PenguinDungeons.getInstance().getPDConfigEntityRegistry().allBase().stream()
                .filter(customEntity -> subject.hasPermission(customEntity.getPermission()))
                .map(PDEntityType::getSpawnEgg)
                .collect(Collectors.toList());

        ItemStack switchToConfig = ItemStack.builder()
                .itemType(ItemTypes.REDSTONE)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.WHITE, "Switch to config added entities"))
                .build();

        ItemStack switchToBuiltIn = ItemStack.builder()
                .itemType(ItemTypes.GOLD_INGOT)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.WHITE, "Switch to built-in entities"))
                .build();

        final ScrollableInventoryList.ClickListener onClick = ((player, itemStack, index, shift) -> {
            if (itemStack.isEmpty()) {
                return;
            }
            player.getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(MainPlayerInventory.class)).offer(itemStack.copy());
        });

        final DualScrollableInventoryList dualScrollableInventoryList = new DualScrollableInventoryList(
                new ScrollableInventoryListInfo(
                        Text.of(TextColors.BLUE, "Choose Built-In Custom Entities!"),
                        builtInCustomEntities,
                        onClick
                ),
                new ScrollableInventoryListInfo(
                        Text.of(TextColors.BLUE, "Choose Config Custom Entities!"),
                        configCustomEntities,
                        onClick
                ),
                switchToConfig,
                switchToBuiltIn
        );

        return dualScrollableInventoryList.create();
    }
}
