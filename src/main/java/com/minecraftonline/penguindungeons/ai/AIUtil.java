package com.minecraftonline.penguindungeons.ai;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityShulker;
import net.minecraft.entity.projectile.EntityShulkerBullet;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.AITaskType;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.golem.Shulker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class AIUtil {

    public static void swapShulkerAttackForCustom(Shulker shulker, ShulkerBulletCreator shulkerBulletCreator, AITaskType aiTaskType) {
        EntityShulker mcShulker = (EntityShulker) shulker;
        EntityShulker.AIAttack delegate = mcShulker.new AIAttack() {
            @Override
            public void updateTask() {
                AtomicInteger atomicInteger = new AtomicInteger(this.attackTime);
                AIUtil.shulkerAttackWithCustomBullet(mcShulker, atomicInteger, shulkerBulletCreator);
                this.attackTime = atomicInteger.get();
            }
        };

        Goal<Agent> normalGoals = shulker.getGoal(GoalTypes.NORMAL).get();
        AIUtil.removeShulkerAttack(normalGoals);


        normalGoals.addTask(4, new DelegatingToMCAI<Shulker>(aiTaskType, delegate));
    }

    public static void removeShulkerAttack(Goal<Agent> goal) {
        goal.getTasks().stream().filter(aiTask -> aiTask instanceof EntityShulker.AIAttack)
                .findAny().ifPresent(task -> goal.removeTask((AITask<? extends Agent>) task));
    }

    public interface ShulkerBulletCreator {
        EntityShulkerBullet create(World world, EntityLivingBase owner, EntityLivingBase target, EnumFacing.Axis axis);

        static ShulkerBulletCreator forCustom(Consumer<EntityLivingBase> onHit) {
            return (world, owner, target, axis) -> new ShulkerBulletCollideEffect(world, owner, target, axis, onHit);
        }

        static ShulkerBulletCreator forCustomEffect(PotionEffect potionEffect) {
            return forCustom(forHit(potionEffect));
        }

        static Consumer<EntityLivingBase> forHit(PotionEffect potionEffect) {
            return entity -> {
                Entity spongeEntity = (Entity)entity;
                List<PotionEffect> effects = spongeEntity.get(Keys.POTION_EFFECTS).orElse(new ArrayList<>());
                effects.add(potionEffect);
                spongeEntity.offer(Keys.POTION_EFFECTS, effects);
            };
        }

        static ShulkerBulletCreator forDirectSnowFlake(EntityLivingBase shooter, Consumer<EntityLivingBase> onHit, double accelX, double accelY, double accelZ)
        {
            return (world, owner, target, axis) -> new ShulkerBulletDirect(world, owner, target, axis, onHit, accelX, accelY, accelZ);
        }
    }

    public static void shulkerAttackWithCustomBullet(EntityShulker mcShulker, AtomicInteger attackTime, ShulkerBulletCreator shulkerBulletCreator) {
        if (mcShulker.world.getDifficulty() != EnumDifficulty.PEACEFUL) {
            int attackTimeCur = attackTime.decrementAndGet();
            EntityLivingBase entitylivingbase = mcShulker.getAttackTarget();
            mcShulker.getLookHelper().setLookPositionWithEntity(entitylivingbase, 180.0F, 180.0F);
            double d0 = mcShulker.getDistanceSq(entitylivingbase);
            if (d0 < 400.0D) {
                if (attackTimeCur <= 0) {
                    attackTime.set(20 + mcShulker.world.rand.nextInt(10) * 20 / 2);
                    EntityShulkerBullet entityshulkerbullet = shulkerBulletCreator.create(mcShulker.world, mcShulker, entitylivingbase, mcShulker.getAttachmentFacing().getAxis());
                    mcShulker.world.spawnEntity(entityshulkerbullet);
                    mcShulker.playSound(SoundEvents.ENTITY_SHULKER_SHOOT, 2.0F, (mcShulker.world.rand.nextFloat() - mcShulker.world.rand.nextFloat()) * 0.2F + 1.0F);
                }
            } else {
                mcShulker.setAttackTarget(null);
            }
        }
    }
}
