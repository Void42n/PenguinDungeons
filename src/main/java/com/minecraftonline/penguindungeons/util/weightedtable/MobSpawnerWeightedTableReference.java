package com.minecraftonline.penguindungeons.util.weightedtable;

import com.minecraftonline.penguindungeons.customentity.PDArchetypeEntityType;
import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.util.EntityUtil;
import org.spongepowered.api.block.tileentity.MobSpawner;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.EntityArchetype;
import org.spongepowered.api.util.weighted.TableEntry;
import org.spongepowered.api.util.weighted.WeightedObject;
import org.spongepowered.api.util.weighted.WeightedTable;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class MobSpawnerWeightedTableReference implements WeightedTableReference<PDEntityType> {

    private final Location<World> mobSpawnerLoc;

    public MobSpawnerWeightedTableReference(Location<World> mobSpawnerLoc) {
        this.mobSpawnerLoc = mobSpawnerLoc;
    }

    private MobSpawner getMobSpawner() throws ReferenceNoLongerValid {
        return this.mobSpawnerLoc.getTileEntity()
                .filter(tileEntity -> tileEntity instanceof MobSpawner)
                .map(tileEntity -> (MobSpawner) tileEntity)
                .orElseThrow(() -> new ReferenceNoLongerValid("Mob spawner no longer exists!"));
    }

    @Nullable
    @Override
    public Double getWeight(PDEntityType obj) throws ReferenceNoLongerValid {
        final Optional<EntityArchetype> optArchetype = obj.getEntityArchetype();
        if (!optArchetype.isPresent()) {
            return null;
        }
        final MobSpawner mobSpawner = getMobSpawner();
        for (TableEntry<EntityArchetype> tableEntry : mobSpawner.possibleEntitiesToSpawn()) {
            // Sponge also does instanceof checks, we don't really know how to deal with the others..
            if (tableEntry instanceof WeightedObject) {
                WeightedObject<EntityArchetype> weightedObject = (WeightedObject<EntityArchetype>) tableEntry;
                if (EntityUtil.areArchetypesEqual(weightedObject.get(), optArchetype.get())) {
                    return tableEntry.getWeight();
                }
            }
        }
        return null;
    }

    @Override
    public void put(PDEntityType obj, double weight) throws ReferenceNoLongerValid, KeyNotValidException {
        if (weight <= 0) {
            throw new IllegalArgumentException("Weight must be greater than 0!");
        }
        final EntityArchetype archetype = obj.getEntityArchetype()
                .orElseThrow(() -> new KeyNotValidException("Key cannot be transformed into an entity archetype so it cannot be put into a spawner!"));

        this.remove(obj);
        MobSpawner mobSpawner = getMobSpawner();
        WeightedTable<EntityArchetype> weightedTable = mobSpawner.possibleEntitiesToSpawn().get();
        weightedTable.add(new WeightedObject<>(archetype, weight));
        mobSpawner.offer(Keys.SPAWNER_ENTITIES, weightedTable);
    }

    @Override
    public boolean has(PDEntityType obj) throws ReferenceNoLongerValid {
        return getWeight(obj) != null;
    }

    @Override
    public void remove(PDEntityType obj) throws ReferenceNoLongerValid {
        Optional<EntityArchetype> optArchetype = obj.getEntityArchetype();
        if (!optArchetype.isPresent()) {
            return;
        }
        final MobSpawner mobSpawner = getMobSpawner();
        WeightedTable<EntityArchetype> weightedTable = mobSpawner.possibleEntitiesToSpawn().get();
        final Iterator<TableEntry<EntityArchetype>> iterator = weightedTable.iterator();
        while (iterator.hasNext()) {
            TableEntry<EntityArchetype> tableEntry = iterator.next();
            if (tableEntry instanceof WeightedObject) {
                WeightedObject<EntityArchetype> weightedObject = (WeightedObject<EntityArchetype>) tableEntry;
                if (EntityUtil.areArchetypesEqual(weightedObject.get(), optArchetype.get())) {
                    iterator.remove();
                }
            }
        }
        mobSpawner.offer(Keys.SPAWNER_ENTITIES, weightedTable);
    }

    @Override
    public Collection<Map.Entry<PDEntityType, Double>> entries() throws ReferenceNoLongerValid {
        return getMobSpawner().possibleEntitiesToSpawn().get().stream()
                .filter(entry -> entry instanceof WeightedObject)
                .map(entry -> (WeightedObject<EntityArchetype>) entry)
                .map(MobSpawnerWeightedTableReference::convert)
                .collect(Collectors.toList());
    }

    @Override
    public double totalWeight() throws ReferenceNoLongerValid {
        return getMobSpawner().possibleEntitiesToSpawn().get().stream()
                .mapToDouble(TableEntry::getWeight)
                .sum();
    }

    private static Map.Entry<PDEntityType, Double> convert(WeightedObject<EntityArchetype> weightedObject) {
        return new AbstractMap.SimpleImmutableEntry<>(new PDArchetypeEntityType(weightedObject.get()), weightedObject.getWeight());
    }
}
