package com.minecraftonline.penguindungeons.mobspawner;

import java.util.Optional;

import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandType;
import org.spongepowered.api.entity.EntityArchetype;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.util.weighted.WeightedSerializableObject;
import org.spongepowered.api.util.weighted.WeightedTable;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.minecraftonline.penguindungeons.PenguinDungeons;

public class MobSpawnerListener {

    public static final String BASE_PERMISSION = PenguinDungeons.BASE_PERMISSION + "mob_spawner.";

    @Listener(order = Order.POST)
    public void onSpawnerEggUse(InteractBlockEvent.Secondary event, @First Player player)
    {
        Optional<ItemStackSnapshot> itemOptional = event.getContext().get(EventContextKeys.USED_ITEM);
        Optional<HandType> hand = event.getContext().get(EventContextKeys.USED_HAND);
        if (!itemOptional.isPresent() || !hand.isPresent()) return;

        ItemStackSnapshot itemSnapshot = itemOptional.get();
        if (itemSnapshot.getType() != ItemTypes.SPAWN_EGG) return;

        Location<World> block = event.getTargetBlock().getLocation().get();
        if (block.getBlockType() != BlockTypes.MOB_SPAWNER) return;

        // always cancel this event here as it is either being replaced or denied
        event.setCancelled(true);

        if (!player.hasPermission(BASE_PERMISSION + "set")) return;

        Optional<EntityType> entityType = itemSnapshot.get(Keys.SPAWNABLE_ENTITY_TYPE);
        if (!entityType.isPresent()) return;

        Optional<DataView> entityTag = itemSnapshot.toContainer().getView(DataQuery.of("UnsafeData", "EntityTag"));
        if (!entityTag.isPresent()) return;

        EntityArchetype entity = EntityArchetype.of(entityType.get());
        entity.setRawData(entityTag.get());

        Optional<WeightedTable<EntityArchetype>> optionalSpawnTable = Optional.empty();
        Optional<Boolean> isSneaking = player.get(Keys.IS_SNEAKING);
        if (isSneaking.isPresent() && isSneaking.get())
        {
            // sneak interact adds the entity to the spawn table instead of replacing it
            optionalSpawnTable = block.get(Keys.SPAWNER_ENTITIES);
        }

        WeightedTable<EntityArchetype> spawnTable;
        // use existing table
        spawnTable = optionalSpawnTable.orElseGet(WeightedTable::new);
        spawnTable.add(entity, 1);

        block.offer(Keys.SPAWNER_ENTITIES, spawnTable);
        block.offer(Keys.SPAWNER_NEXT_ENTITY_TO_SPAWN, new WeightedSerializableObject<>(entity, 1));

        if (player.gameMode().get() != GameModes.CREATIVE)
        {
            // use up the item
            ItemStack item = itemSnapshot.createStack();
            item.setQuantity(item.getQuantity() - 1);
            player.setItemInHand(hand.get(), item);
        }
    }

}
